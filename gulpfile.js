import gulp from "gulp"
import * as sass from "sass"
import gulpsass from "gulp-sass"
import browserSync from "browser-sync"
import htmlmin from "gulp-htmlmin"
import imagemin from "gulp-imagemin"
import cleanCSS from "gulp-clean-css"
import autoprefixer from "gulp-autoprefixer"
// import validator from "gulp-html"


const sassPlugin = gulpsass(sass)
const browser = browserSync.create()


export function html() {
    return gulp.src("src/index.html")
        // .pipe(validator())
        .pipe(htmlmin({collapseWhitespace:true}))
        .pipe(gulp.dest("build"))
}

export function css() {
    return gulp.src("src/scss/**/*.scss")
        .pipe(sassPlugin())
        .pipe(autoprefixer({
			cascade: false
		}))
        .pipe(cleanCSS())
        .pipe(gulp.dest("build/css"))
}

function reload(cb) {
    browser.reload()
    cb()
}

export function images() {
    return gulp.src("src/images/*")
        .pipe(imagemin())
        .pipe(gulp.dest("build/images"))
}



export function watch() {
    browser.init({
        server: {
            baseDir: "build"
        }
    })
    return gulp.watch("src/**/*.*", gulp.series(gulp.parallel(html, css, images), reload))
}


export default gulp.parallel(html, css, images)